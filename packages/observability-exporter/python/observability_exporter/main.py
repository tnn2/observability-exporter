# -*- mode: python; python-indent: 4 -*-
"""Main NSO package module

This is the entry point for the NSO package, it is invoked by NSO on startup
and sets up our ptrace processing pipeline based on configuration read from
NSO.
"""
import logging
import multiprocessing

from influxdb import InfluxDBClient

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import (
    OTLPSpanExporter as grpcOTLPSpanExporter,
)
from opentelemetry.exporter.otlp.proto.http.trace_exporter import (
    OTLPSpanExporter as httpOTLPSpanExporter,
)
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleSpanProcessor

import ncs
import _ncs

from . import background_process
from . import dictsubscriber
from . import ptrace

# InfluxDBClient uses urllib3 which is quite chatty
logging.getLogger("urllib3").setLevel(logging.WARNING)


def decrypt_string(node, value):
    """Decrypts an encrypted tailf:aes-cfb-128-encrypted-string type leaf

    :param node: any maagic Node
    :param value: the encrypted leaf value
    """
    if value is None:
        return None

    if isinstance(node._backend, ncs.maagic._TransactionBackend):
        node._backend.maapi.install_crypto_keys()
    elif isinstance(node._backend, ncs.maagic._MaapiBackend):
        node._backend.install_crypto_keys()
    else:
        raise ValueError("Unknown MaagicBackend for leaf")

    return _ncs.decrypt(value)  # pylint: disable=no-member


def NotifReader(qbuf):
    """Read notifications from NSO API and put on q"""
    with ncs.maapi.single_write_trans("observability-exporter", "system") as t:
        root = ncs.maagic.get_root(t)
        export_config = root.progress.export
        cfg_include_diffset = bool(export_config.include_diffset)

        if "ptrace-export-diffset-reader" not in root.kickers.data_kicker:
            kicker_diffset = root.kickers.data_kicker.create(
                "ptrace-export-diffset-reader"
            )
            kicker_diffset.monitor = (
                "/progress:progress/observability-exporter:export/include-diffset"
            )
            kicker_diffset.kick_node = "./.."
            kicker_diffset.action_name = "restart-reader"

            t.apply()

        # While not super scientific, the 200k value wasn't entirely picked out
        # of thin air.  We've seen how NSO can produce 35k events per second,
        # typically during device interaction heavy segments (like small
        # changesets to many devices). Our processing pipeline can normally do
        # something on the order of 30k-40k events per second on a fast CPU but
        # there are also bad cases when this drops. Had a scenario where it went
        # all the way down to 1k events per second (now fixed). To handle such a
        # burst and "work it off" during an extended time period, we have a
        # pretty hefty buffer.
        ptrace.q_putter(ptrace.notifs_reader(cfg_include_diffset), qbuf, 200000)


def ObservabilityExporter(qbuf):
    """Function for running as NSO background worker to subscribe to NSO
    progress-trace notification and export to tracing system
    """
    log = logging.getLogger("observability-exporter")
    log.info("Observablity progress-trace exporter started")

    conf = dictsubscriber.DictSubscriber(
        app=None,
        log=log,
        subscriptions=[
            (
                "jaeger_base_url",
                "/progress:progress/observability-exporter:export/jaeger-base-url",
            ),
            (
                "extra_tags",
                "/progress:progress/observability-exporter:export/extra-tags",
            ),
        ],
    )

    metrics_q = []
    need_kicker_conf = False

    # Read in configuration from CDB
    with ncs.maapi.single_read_trans("observability-exporter", "system") as t:
        root = ncs.maagic.get_root(t)
        export_config = root.progress.export

        cfg_influxdb = export_config.influxdb.exists()
        if cfg_influxdb:
            if ptrace.is_ipv6(export_config.influxdb.host):
                influx_host = f"[{export_config.influxdb.host}]"
            else:
                influx_host = export_config.influxdb.host

            influx_client = InfluxDBClient(
                influx_host,
                export_config.influxdb.port,
                export_config.influxdb.username,
                decrypt_string(root, export_config.influxdb.password),
                export_config.influxdb.database,
            )
            influx_exporter = ptrace.InfluxdbExporter(influx_client, metrics_q)
            influx_exporter.start()

        cfg_otlp = export_config.otlp.exists()
        if cfg_otlp:
            if ptrace.is_ipv6(export_config.otlp.host):
                otlp_host = f"[{export_config.otlp.host}]"
            else:
                otlp_host = export_config.otlp.host

            if export_config.otlp.transport == "http":
                log.info("Setting up otlp http exporter")
                otlp_exporter = httpOTLPSpanExporter(
                    endpoint=f"http://{otlp_host}:{export_config.otlp.port or 4318}/v1/traces",
                )
            elif export_config.otlp.transport == "grpc":
                log.info("Setting up otlp grpc exporter")
                otlp_exporter = grpcOTLPSpanExporter(
                    endpoint=f"{otlp_host}:{export_config.otlp.port or 4317}",
                    insecure=True,
                )
            else:
                raise NotImplementedError(
                    f"Not a supported transport {export_config.otlp.transport}"
                )

            trace.set_tracer_provider(
                TracerProvider(resource=Resource.create({SERVICE_NAME: "NSO"}))
            )
            trace.get_tracer_provider().add_span_processor(
                SimpleSpanProcessor(otlp_exporter)
            )

            tracer = trace.get_tracer("observability-exporter")

        if (
            "ptrace-export-influxdb" not in root.kickers.data_kicker
            or "ptrace-export-otlp" not in root.kickers.data_kicker
        ):
            need_kicker_conf = True

    # install kickers to restart ourselves on configuration changes
    if need_kicker_conf:
        with ncs.maapi.single_write_trans(
            "observability-exporter-kicker", "system"
        ) as t:
            root = ncs.maagic.get_root(t)
            kicker_influx = root.kickers.data_kicker.create("ptrace-export-influxdb")
            kicker_influx.monitor = (
                "/progress:progress/observability-exporter:export/influxdb"
            )
            kicker_influx.kick_node = "./.."
            kicker_influx.action_name = "restart"

            kicker_otlp = root.kickers.data_kicker.create("ptrace-export-otlp")
            kicker_otlp.monitor = (
                "/progress:progress/observability-exporter:export/otlp"
            )
            kicker_otlp.kick_node = "./.."
            kicker_otlp.action_name = "restart"

            # Delete a kicker installed by previous versions of this package
            try:
                root.kickers.data_kicker.delete("ptrace-export-diffset")
                root.kickers.data_kicker.delete("ptrace-export-jaeger")
            # pylint:disable=broad-except
            except Exception:
                pass

            t.apply()

    stream = ptrace.get_pipeline(ptrace.q_getter(qbuf))

    # export to otlp collector via opentelemetry library
    if cfg_otlp:
        stream = ptrace.export_otel(tracer, stream)

    # export to influxdb
    if cfg_influxdb:
        stream = ptrace.export_influx_span(metrics_q, stream, conf=conf)
        stream = ptrace.export_influx_span_count(metrics_q, stream, conf=conf)
        stream = ptrace.export_influx_tlock(metrics_q, stream, conf=conf)
        stream = ptrace.export_influx_transaction(metrics_q, stream, conf=conf)

    # dummy consumer
    for event in stream:
        pass


class Main(ncs.application.Application):
    nreader = None
    worker = None

    def setup(self):
        qbuf = multiprocessing.Manager().Queue()
        self.nreader = background_process.Process(
            self,
            NotifReader,
            (qbuf,),
            config_path="/progress:progress/observability-exporter:export/enabled",
            run_during_upgrade=True,
        )
        self.worker = background_process.Process(
            self,
            ObservabilityExporter,
            (qbuf,),
            config_path="/progress:progress/observability-exporter:export/enabled",
            run_during_upgrade=True,
        )
        self.register_action(
            "ptrace-export-restart",
            background_process.RestartWorker,
            init_args=self.worker,
        )
        self.register_action(
            "ptrace-reader-restart",
            background_process.RestartWorker,
            init_args=self.nreader,
        )
        self.nreader.start()
        self.worker.start()

    def teardown(self):
        self.nreader.stop()
        self.worker.stop()
